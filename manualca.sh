#!/bin/bash

#WD=$(pwd)
WD=$(dirname "$(readlink -f "$0")")
DATA_PATH="/mnt/cryptramdisk"
XDB_FILE="rootca.xdb"

. "${WD}/lib/logging"
. "${WD}/lib/zenity"
. "${WD}/lib/encrypted_ramdisk"
. "${WD}/lib/xca_file"
. "${WD}/lib/quorum"


SELECTED_ITEM=$(zenity --list \
  --title="Select an Offline CA functionality to execute" \
  --radiolist \
  --width=400 \
  --height=230 \
  --column "Pick" --column "File type" \
  FALSE "Configure Date / Time" \
  FALSE "Load existing CA configuration" \
  FALSE "Split and save CA configuration")

case "${SELECTED_ITEM}" in
  "Configure Date / Time" )
    exec_date_time_config
    ;;
  "Load existing CA configuration" )
    exec_crypto_ramdisk
    exec_xca_config_load
    exec_start_xca
    ;;
  "Split and save CA configuration" )
    exec_split_xdb
    ;;
  * )
    zen_warning "Nothing selected. Exiting quietly."
    ;;
esac
