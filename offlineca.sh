#!/bin/bash

#WD=$(pwd)
WD=$(dirname "$(readlink -f "$0")")
DATA_PATH="/mnt/cryptramdisk"
XDB_FILE="rootca.xdb"

. "${WD}/lib/logging"
. "${WD}/lib/zenity"
. "${WD}/lib/encrypted_ramdisk"
. "${WD}/lib/xca_file"
. "${WD}/lib/quorum"

exec_crypto_ramdisk

exec_date_time_config

exec_xca_config_load

exec_start_xca

zen_message "Now, after all the CA operations have been performed, we will start with splitting the XCA database file into multiple parts.\n\nYou will be asked to define the number of parts there will be, and the minimal number of these parts which are required to rebuild the XCA database (quorum)."

exec_split_xdb
