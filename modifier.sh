#!/bin/bash

apt update && apt upgrade -y && apt install -y xca libgfshare-bin zenity && apt clean

cd /tmp

[[ -e master.tar.gz ]] && rm master.tar.gz

wget -O /tmp/master.tar.gz https://bitbucket.org/dserfez/offline_ca/get/master.tar.gz

tar xzvf master.tar.gz -C /opt/

mv /opt/dserfez-offline_ca-* /opt/offline_ca

# Run Offline CA wizard after booting
echo "sudo /opt/offline_ca/offlineca.sh" >> /etc/xdg/lxsession/Lubuntu/autostart

# Disable "open with..." dialog on media insertion
sed -i 's|autorun=1|autorun=0|' /etc/xdg/pcmanfm/lubuntu/pcmanfm.conf

# Remove unwanted applets from autoloading
UNWANTED_AUTORUNS="update-notifier pulseaudio nm-applet light-locker blueman gnome-software-service print-applet"
for UNW_AR in ${UNWANTED_AUTORUNS} ; do
  rm "/etc/xdg/autostart/${UNW_AR}.desktop"
done

# Put Offline CA shortcuts on the Desktopr
sed -i 's|for file in /usr/share/applications/ubiquity.desktop /usr/share/applications/kde4/ubiquity-kdeui.desktop; do|for file in /opt/offline_ca/manualca.desktop /opt/offline_ca/offlineca.desktop  /usr/share/applications/kde4/ubiquity-kdeui.desktop; do|' \
/usr/share/initramfs-tools/scripts/casper-bottom/25adduser
sed -i 's|break||' /usr/share/initramfs-tools/scripts/casper-bottom/25adduser

MD5_25ADDUSER=$(md5sum "${work_dir}/usr/share/initramfs-tools/scripts/casper-bottom/25adduser" | awk '{print $1}')
sudo sed -i "s|.*  usr/share/initramfs-tools/scripts/casper-bottom/25adduser|${MD5_SPLASH}  usr/share/initramfs-tools/scripts/casper-bottom/25adduser|" /var/lib/dpkg/info/casper.md5sums
