# Bootable Offline CA

Bootable Lubuntu with [XCA](http://xca.sourceforge.net/) installed, and helpers to make it secure enough for an offline CA.

It is secure because:

* Creates encrypted ramdisk for storage of XCA .xdb file (procedure shamelessly stolen from https://www.backtrack-linux.org/forums/showthread.php?t=42033)
* Root key material (stored in XCA .xdb file) by default, never leaves the system in one paece, but split into N parts of M, from which N parts of M are required to reconstruct the XCA .xdb file.

It is secure if:
* No private keys are exported to files
* XCA database file \*.xdb is saved only to the encrypted ramdisk (/mnt/cryptramdisk)
* The system running Offline_CA is not connected to any network

## Prerequisites
* x86 based computer that is capable of booting from CD or USB
    * not connected to any network
    * no need for a HDD connected/inside
    * if it has at least 2GB of RAM, then the boot media can be removed after booting
* Offline_CA bootable media
* removable media for XCA database file parts, CSRs, certificates, CRL storage


## CA Workflow
* Boot Offline_CA bootable media on an x86 computer
    * On boot you are prompted for language - English is default, change using arrow keys if necessary, and press Return when done.
    * You can select a different keymap from the default Enlish, using the F3 key.
* Date and time configuration utility is started automatically
* Set date and time if necessary
* After exiting date and time configuration utility, you are prompted for an existing XCA database file as single file or split into parts
* Load XCA database file / parts
* Perform PKI operations with XCA (out of scope of this README, more information on how to use it can be found [here](http://xca.sourceforge.net/xca-14.html))
* Export certificates and CRLs as \*.crl, \*.cer, \*.pem, \*.p7b to /mnt/cryptramdisk
* After exiting XCA you are prompted to enter the number of parts into which will the \*.xdb be split (SHARECOUNT) and the minimal number of required parts to reconstruct the \*.xdb (THRESHOLD) - the quorum.
* When the numbre of file parts is defined, you will be prompted to connect external media on which the each file part will be saved, together with all the exported \*.crl, \*.cer, \*.pem, \*.p7b files and checksums


## Build ISO using Customizer GUI
To make the Bootable CA remix, I found the easiest for me to use https://github.com/kamilion/customizer
There is enough information and good guide and advice on the project home page.

In order to build the Offline_CA ISO:
* Start Customizer GUI
* Use the "Select ISO" button to select the Lubuntu iso image which is then extracted automatically
* Set the "Live user" and "Hostname" both to: rootca
* "Execute Hook" and select the modifier.sh from this repo
* "Rebuild ISO"

* When the ISO is selected and extracted, select the hook file modifier.sh from this repo and execute it (of course, you will check it first and not just execute blindly something downloaded from the internet).

wget https://bitbucket.org/dserfez/offline_ca/raw/3883ab4ad6df96798a16fd7eda14e5ae4df80986/modifier.sh

sudo customizer -k modifier.sh

## (Re)build the ISO using cli
When the /etc/customizer.conf is configured for you, yuo can just run the "(re)build sequence":

`sudo customizer --clean && sudo customizer --extract && sudo customizer --hook && sudo customizer --rebuild`

## Manual install

in the customizer console update and install necessary packages:
`apt update && apt upgrade -y && apt install -y xca libgfshare-bin zenity && apt clean`

* Extract this project (https://bitbucket.org/dserfez/offline_ca/get/master.tar.gz) somewhere on the remix root i.e. /opt/offline_ca

* Add full path of offlineca.sh on the Bootable CA remix to file /etc/xdg/lxsession/Lubuntu/autostart in order to be ran automatically after system boot

* Rebuild the ISO and use it
