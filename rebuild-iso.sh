#!/bin/bash

work_dir=$(grep ^work_dir /etc/customizer.conf | tail -n 1 | awk '{print $3}')

time sudo customizer --clean
time sudo customizer --extract
time sudo customizer --hook

sudo cp splash.png "${work_dir}/ISO/isolinux/splash.png"
sudo sed -i 's|Try Lubuntu without installing|Boot Offline CA|' "${work_dir}/ISO/isolinux/txt.cfg"
sudo sed -i 's|append  file=/cdrom/preseed/lubuntu.seed boot=casper initrd=/casper/initrd.lz quiet splash ---|append  file=/cdrom/preseed/lubuntu.seed boot=casper initrd=/casper/initrd.lz quiet splash toram ---|' "${work_dir}/ISO/isolinux/txt.cfg"
MD5_ISOLINUXTXT=$(md5sum "${work_dir}/ISO/isolinux/txt.cfg" | awk '{print $1}')
MD5_SPLASH=$(md5sum "${work_dir}/ISO/isolinux/splash.png" | awk '{print $1}')
sudo sed -i "s|.*  ./isolinux/splash.png|${MD5_SPLASH}  ./isolinux/splash.png|" "${work_dir}/ISO/md5sum.txt"
sudo sed -i "s|.*  ./boot/grub/loopback.cfg|${MD5_ISOLINUXTXT}  ./isolinux/txt.cfg|" "${work_dir}/ISO/md5sum.txt"

time sudo customizer --rebuild

time sudo isohybrid "${work_dir}/Ubuntu-i386-17.04.iso"
